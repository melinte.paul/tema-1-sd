#ifndef FOOTBALL_CLUB_H_D
#define FOOTBALL_CLUB_H_

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


// List node which contains information about one football player.
typedef struct Player {
	char *name;					// player's name
	char *position;				// player's game position
	int score;					// player's score
	int injured;				// indicator for injury (1 true, 0 false)
	struct Player *next;		// next list node
	struct Player *prev;		// previous list node
} Player;

// Structure which contains information about one football club.
typedef struct FootballClub {
	char *name;					// club's name
	Player *players;			// list with players in good shape
	Player *injured_players;	// list with injured players
	struct FootballClub *next;	// next list node
} FootballClub;

char* strcpy_d(char *s)
{
	char* c = malloc(strlen(s)+1);
	strcpy(c,s);
	return c;
}

FootballClub* add_club_next(FootballClub *club, char *name)
{
	FootballClub* new_club = malloc(sizeof(FootballClub));
	new_club->name=strcpy_d(name);
	new_club->players=NULL;
	new_club->injured_players=NULL;
	new_club->next=club->next;
	club->next=new_club;

	return new_club;
}

FootballClub *initialize_clubs(int clubs_no, char **names) {
	if(clubs_no<=0)
	{
		return NULL;
	}

	FootballClub *new_clubs = malloc (sizeof(FootballClub));
	new_clubs->name=strcpy_d(names[0]);
	new_clubs->players=NULL;
	new_clubs->injured_players=NULL;
	new_clubs->next=NULL;

	FootballClub* new_club=new_clubs;

	int i;
	for(i=1;i<clubs_no;i++)
	{
		new_club = add_club_next(new_club,names[i]);
	}

	return new_clubs;
}

FootballClub *add_club(FootballClub *clubs, char *name) {
	FootballClub* club = clubs;

	if(clubs == NULL)
	{
		return initialize_clubs(1, &name);
	}

	while(club->next!=NULL)
	{
		club = club->next;
	}

	add_club_next(club,name);

	return clubs;
}

void add_player_list(Player** players, int injured, char *player_name, char *position, int score,
				int(*cmp)(Player* ,Player*))
{
	Player* new_player = malloc(sizeof(Player));
	new_player->name = strcpy_d(player_name);
	new_player->position = strcpy_d(position);
	new_player->score = score;
	new_player->injured = injured;
	Player* players_it = *players;

	if(cmp(new_player,players_it) || players_it == NULL)
	{
		new_player->next = players_it;
		new_player->prev = NULL;
		if(players_it != NULL)
		{
			players_it->prev=new_player;
		}
		*players = new_player;
		return;
	}

	while(cmp(new_player,players_it->next) == 0 && players_it->next != NULL)
	{
		players_it = players_it->next;
	}

	new_player->next = players_it->next;
	new_player->prev = players_it;
	if(players_it->next!=NULL)
	{
		players_it->next->prev=new_player;
	}
	players_it->next=new_player;
}

int cmp_add_player(Player* p1, Player* p2)
{
	if(p2 == NULL)
	{
		return 1;
	}

	int cmp_poz = strcmp(p1->position,p2->position);
	int cmp_name = strcmp(p1->name,p2->name);

	if(cmp_poz < 0)
		return 1;
	else if(cmp_poz == 0)
	{
		if(p1->score > p2->score)
			return 1;
		else if(p1->score == p2->score)
		{
			if(cmp_name < 0)
				return 1;
		}
	}

	return 0;
}

FootballClub* get_club(FootballClub* clubs, char *club_name)
{
	FootballClub* clubs_it = clubs;

	while(clubs_it !=NULL && strcmp(clubs_it->name,club_name) != 0)
	{
		clubs_it = clubs_it->next;
	}

	return clubs_it;
}

void add_player(FootballClub *clubs, char *club_name, 
				char *player_name, char *position, int score) {
	FootballClub* clubs_it = get_club(clubs,club_name);

	if(clubs_it == NULL)
	{
		return;
	}

	add_player_list(&(clubs_it->players),0,player_name,position,score,cmp_add_player);
}

void remove_player_list(Player** players, char* player_name)
{
	Player* removed = *players;
	if(removed == NULL)
	{
		return;
	}

	if(strcmp((*players)->name, player_name) == 0)
	{
		if((*players)->next != NULL)
			(*players)->next->prev = NULL;
		(*players) = (*players)->next;
		free(removed->name);
		free(removed->position);
		free(removed);
		return;
	}

	while(removed != NULL)
	{
		if(strcmp(removed->name,player_name) == 0)
		{
			removed->prev->next = removed->next;
			if(removed->next != NULL)
			{
				removed->next->prev = removed->prev;
			}
			free(removed->name);
			free(removed->position);
			free(removed);
			return;
		}
		removed = removed->next;
	}
}

void remove_player(FootballClub *clubs, char *club_name, char *player_name) {
	FootballClub* clubs_it = get_club(clubs,club_name);

	if(clubs_it == NULL)
	{
		return;
	}

	remove_player_list(&(clubs_it->players), player_name);
	remove_player_list(&(clubs_it->injured_players), player_name);
}

int cmp_add_player_injury(Player* p1, Player* p2)
{
	if(p2 == NULL)
	{
		return 1;
	}

	int cmp_name = strcmp(p1->name,p2->name);
	if(cmp_name < 0)
		return 1;
	return 0;
}

void update_score(FootballClub *clubs, char *club_name, 
					char *player_name, int score) {
	clubs = get_club(clubs,club_name);

	Player* player_it = clubs->players;

	while(player_it != NULL)
	{
		if(strcmp(player_it->name,player_name) == 0)
		{
			char*pos = strcpy_d(player_it->position);
			remove_player_list(&(clubs->players),player_name);
			add_player_list(&(clubs->players),0,player_name,pos,score,cmp_add_player);
			free(pos);
			return;
		}
		player_it = player_it->next;
	}

	player_it = clubs->injured_players;
	while(player_it != NULL)
	{
		if(strcmp(player_it->name,player_name) == 0)
		{
			char*pos = strcpy_d(player_it->position);
			remove_player_list(&(clubs->injured_players),player_name);
			add_player_list(&(clubs->injured_players),1,player_name,pos,score,cmp_add_player_injury);
			free(pos);
			return;
		}
		player_it = player_it->next;
	}
}

void update_game_position(FootballClub *clubs, char *club_name, 
							char *player_name, char *position, int score) {
	clubs = get_club(clubs,club_name);

	Player* player_it = clubs->players;
	while(player_it != NULL)
	{
		if(strcmp(player_it->name,player_name) == 0)
		{
			char*pos = strcpy_d(position);
			remove_player_list(&(clubs->players),player_name);
			add_player_list(&(clubs->players),0,player_name,pos,score,cmp_add_player);
			free(pos);
			return;
		}
		player_it = player_it->next;
	}

	player_it = clubs->injured_players;
	while(player_it != NULL)
	{
		if(strcmp(player_it->name,player_name) == 0)
		{
			char*pos = strcpy_d(player_it->position);
			remove_player_list(&(clubs->injured_players),player_name);
			add_player_list(&(clubs->injured_players),1,player_name,pos,score,cmp_add_player_injury);
			free(pos);
			return;
		}
		player_it = player_it->next;
	}

}

int maxim(int a, int b)
{
	if(a>b)
		return a;
	return b;
}

void add_injury(FootballClub *clubs, char *club_name,
				char *player_name, int days_no) {
	clubs = get_club(clubs,club_name);

	Player* player_it = clubs->players;
	while(player_it != NULL)
	{
		if(strcmp(player_it->name,player_name) == 0)
		{
			add_player_list(&(clubs->injured_players),1,player_it->name,player_it->position,maxim(-100,player_it->score - (float) days_no * 0.1), cmp_add_player_injury);
			remove_player_list(&(clubs->players),player_name);
			return;
		}
		player_it = player_it->next;
	}
}

void recover_from_injury(FootballClub *clubs, char *club_name, 
							char *player_name) {
	clubs = get_club(clubs,club_name);

	Player* player_it = clubs->injured_players;
	while(player_it != NULL)
	{
		if(strcmp(player_it->name,player_name) == 0)
		{
			add_player_list(&(clubs->players),0,player_it->name,player_it->position,player_it->score,cmp_add_player);
			remove_player_list(&(clubs->injured_players),player_name);
			return;
		}
		player_it = player_it->next;
	}
}

void transfer_player(FootballClub *clubs, char *player_name, 
					char *old_club, char *new_club) {
	FootballClub* club1 = get_club(clubs,old_club);
	FootballClub* club2 = get_club(clubs,new_club);

	Player* player_it = club1->players;
	while(player_it != NULL)
	{
		if(strcmp(player_it->name,player_name) == 0)
		{
			add_player_list(&(club2->players),0,player_it->name,player_it->position,player_it->score,cmp_add_player);
			remove_player_list(&(club1->players),player_name);
			return;
		}
		player_it = player_it->next;
	}

	player_it = club1->injured_players;
	while(player_it != NULL)
	{
		if(strcmp(player_it->name,player_name) == 0)
		{
			add_player_list(&(club2->injured_players),1,player_it->name,player_it->position,player_it->score,cmp_add_player);
			remove_player_list(&(club1->injured_players),player_name);
			return;
		}
		player_it = player_it->next;
	}

}

// Frees memory for a list of Player.
void destroy_player_list(Player *player) {
	Player* removed;

	while(player != NULL)
	{
		removed = player;
		player = player->next;

		free(removed->name);
		free(removed->position);
		free(removed);
	}
}

// Frees memory for a list of FootballClub.
void destroy_club_list(FootballClub *clubs) {

	FootballClub* removed;

	while(clubs != NULL)
	{
		removed = clubs;
		clubs = clubs->next;

		free(removed->name);
		destroy_player_list(removed->players);
		destroy_player_list(removed->injured_players);
		free(removed);
	}
}

// Displays a list of players.
void show_list(FILE *f, Player *players, int free_memory) {
	fprintf(f, "P: ");
	Player *player = players;
	while (player) {
		fprintf(f, "(%s, %s, %d, %c) ", 
			player->name,
			player->position,
			player->score,
			player->injured ? 'Y' : '_');
		player = player->next;
	}
	if (free_memory) {
		destroy_player_list(players);
	}
	fprintf(f, "\n");
}

// Displays a list of players in reverse.
void show_list_reverse(FILE *f, Player *players, int free_memory) {
	fprintf(f, "P: ");
	Player *player = players;
	if (player) {
		while (player->next) {
			player = player->next;
		}
		while (player) {
			fprintf(f, "(%s, %s, %d, %c) ", 
				player->name,
				player->position,
				player->score,
				player->injured ? 'Y' : '_');
			player = player->prev;
		}
	}
	if (free_memory) {
		destroy_player_list(players);
	}
	fprintf(f, "\n");
}


// Displays information about a football club.
void show_clubs_info(FILE *f, FootballClub *clubs) {
	fprintf(f, "FCs:\n");
	while (clubs) {
		fprintf(f, "%s\n", clubs->name);
		fprintf(f, "\t");
		show_list(f, clubs->players, 0);
		fprintf(f, "\t");
		show_list(f, clubs->injured_players, 0);
		clubs = clubs->next;
	}
}

#endif // FOOTBALL_CLUB_H_INCLUDED