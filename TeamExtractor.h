
#ifndef TEAM_EXTRACTOR_H_D
#define TEAM_EXTRACTOR_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "FootballClub.h"


Player *union_teams(FootballClub *clubs, char *club_A, char *club_B) {
	FootballClub* club1 = get_club(clubs, club_A);
	FootballClub* club2 = get_club(clubs, club_B);

	Player* team_union = NULL;

	Player* player_it = club1->players;
	while(player_it != NULL)
	{
		add_player_list(&team_union,0,player_it->name,player_it->position,player_it->score,cmp_add_player);
		player_it = player_it->next;
	}

	player_it = club2->players;
	while(player_it != NULL)
	{
		add_player_list(&team_union,0,player_it->name,player_it->position,player_it->score,cmp_add_player);
		player_it = player_it->next;
	}

	return team_union;

}

Player *get_best_player(FootballClub *clubs, char *position) {
	Player* best_player = malloc(sizeof(Player));
	best_player->name = malloc(20*sizeof(char));
	best_player->position = strcpy_d(position);
	best_player->score = -101;
	best_player->prev = best_player->next = NULL;
	best_player->injured = 0;

	while(clubs != NULL)
	{
		Player* player_it = clubs->players;

		while(player_it != NULL)
		{
			if(strcmp(player_it->position,position) == 0)
			{
				if(player_it -> score > best_player->score)
				{
					strcpy(best_player->name, player_it->name);
					best_player->score = player_it->score;
				}
			}
			player_it = player_it->next;
		}
		clubs = clubs->next;
	}

	if(best_player->score == -101)
	{
		free(best_player->name);
		free(best_player->position);
		free(best_player);
		return NULL;
	}

	return best_player;
}

int cmp_best_player(Player* p1, Player* p2)
{
	if(p2 == NULL)
	{
		return 1;
	}

	int cmp_name = strcmp(p1->name,p2->name);

	if(p1->score > p2->score)
		return 1;
	else if(p1->score == p2->score)
	{
		if(cmp_name < 0)
			return 1;
	}
	return 0;
}


Player *get_top_players(FootballClub *clubs, int N) {
	Player* best_players_all = NULL;
	Player* best_players;
	Player* players_it;
	int i;

	while(clubs != NULL)
	{
		best_players = NULL;
		players_it = clubs->players;

		while(players_it != NULL)
		{
			add_player_list(&best_players,0,players_it->name,players_it->position,players_it->score,cmp_best_player);
			players_it = players_it->next;
		}

		players_it = best_players;
		i = N;

		while(players_it != NULL && i > 0)
		{
			add_player_list(&best_players_all,0,players_it->name,players_it->position,players_it->score,cmp_best_player);
			i--;
			players_it = players_it->next;
		}

		destroy_player_list(best_players);
		clubs = clubs->next;
	}

	return best_players_all;
}

Player *get_players_by_score(FootballClub *clubs, int score) {
	Player* team_union = NULL;

	while(clubs != NULL)
	{
		Player* player_it = clubs->players;

		while(player_it != NULL)
		{
			if(player_it->score >= score)
			{
				add_player_list(&team_union,0,player_it->name,player_it->position,player_it->score,cmp_best_player);
			}
			player_it = player_it->next;
		}

		player_it = clubs->injured_players;

		while(player_it != NULL)
		{
			if(player_it->score >= score)
			{
				add_player_list(&team_union,1,player_it->name,player_it->position,player_it->score,cmp_best_player);
			}
			player_it = player_it->next;
		}


		clubs = clubs->next;
	}

	return team_union;
}

Player *get_players_by_position(FootballClub *clubs, char *position) {
	Player* team_union = NULL;

	while(clubs != NULL)
	{
		Player* player_it = clubs->players;

		while(player_it != NULL)
		{
			if(strcmp(player_it->position,position) == 0)
			{
				add_player_list(&team_union,0,player_it->name,player_it->position,player_it->score,cmp_best_player);
			}
			player_it = player_it->next;
		}

		player_it = clubs->injured_players;

		while(player_it != NULL)
		{
			if(strcmp(player_it->position,position) == 0)
			{
				add_player_list(&team_union,1,player_it->name,player_it->position,player_it->score,cmp_best_player);
			}
			player_it = player_it->next;
		}

		clubs = clubs->next;
	}

	return team_union;
}

Player *get_best_team(FootballClub *clubs) {
	Player* best_team = NULL;
	Player* player_it;

	int no_players[] = {1,4,3,3};
	int i;
	Player* best_players[] = {NULL, NULL, NULL, NULL,NULL};

	char* position[] = {"portar", "fundas", "mijlocas", "atacant"};

	while(clubs != NULL)
	{
		player_it = clubs->players;

		while(player_it != NULL)
		{
			for(i=0;i<4;i++)
			{
				if(strcmp(player_it->position, position[i]) == 0)
				{
					add_player_list(&(best_players[i]),0,player_it->name,player_it->position,player_it->score,cmp_best_player);
					break;
				}
			}
			player_it = player_it->next;
		}

		player_it = clubs->injured_players;

		while(player_it != NULL)
		{
			for(i=0;i<4;i++)
			{
				if(strcmp(player_it->position, position[i]) == 0)
				{
					add_player_list(&(best_players[i]),1,player_it->name,player_it->position,player_it->score,cmp_best_player);
					break;
				}
			}
			player_it = player_it->next;
		}
		clubs = clubs->next;
	}

	for(i=0;i<4;i++)
	{
		player_it = best_players[i];

		while(player_it != NULL && no_players[i] > 0)
		{
			add_player_list(&(best_team),player_it->injured,player_it->name,player_it->position,player_it->score,cmp_best_player);
			player_it = player_it->next;
			no_players[i]--;
		}

		destroy_player_list(best_players[i]);
	}

	return best_team;
}

#endif // TEAM_EXTRACTOR_H_INCLUDED